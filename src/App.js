import React from "react";
import './App.css';
import Number from "./Number/Number";
import Numbers from  "./Numbers";

class App extends React.Component {
    state = {
        numbers: Numbers.getNumbers()
    };

    changeNumbers = () => {
        const numbers = Numbers.getNumbers();

        this.setState({numbers});
    };

    render() {
        return (
            <div className="App">
                <div className="btn-gen">
                    <button onClick={this.changeNumbers}>New numbers</button>
                </div>

                {this.state.numbers.map((number, key) => {
                    return (
                    <Number key={key} {...number}/>
                    );
                })}
            </div>
        );
    }
}

export default App;
