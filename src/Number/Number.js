import React from "react";
import './Number.css';

const Number = (props) => {
  return (
      <div className="circles">
          {props.number}
      </div>
  );
};

export default Number;