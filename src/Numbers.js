const rand = (min = 5, max = 36) => {
    return Math.floor(Math.random() * (max - min + 1) + min)
}

const generateRandomNumbers = () => {
    let numbers = [];
    let numbersAmount = 5;
    let bool = true;    

    do {
        let randomNumber = rand();

        if (!numbers.includes(randomNumber)) {
            numbers.push(randomNumber);

            if (numbers.length == numbersAmount) {
                bool = false;
            }
        }
    } while (bool)

    return numbers;
};

export default {
    getNumbers() {
        let data = [];
        let numbers = generateRandomNumbers();

        numbers.sort((a, b) => { return a - b; });

        for (let i = 0; i < numbers.length; i++) {
            data.push({number: numbers[i]});
        }

        return data;
    }
};
